import Vue from 'vue';
// import Main from './app/Main.vue';
import Formbuildvue from './app/Formbuildvue.vue';
import FormioExample from './app/FormioExample.vue';
import FormioPDF from './app/FormioPDF.vue';
import CreateProduct from './app/CreateProduct.vue';
import WizardExample from './app/WizardExample.vue';

import Auth from './app/Auth.vue';
import './index.scss';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VueRouter from 'vue-router';
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue';

// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/formexample'
    },
    {
      path: '/formbuilder',
      component: Formbuildvue
    },
    {
      path: '/formexample',
      component: FormioExample
    },
    {
      path: '/pdfexample',
      component: FormioPDF
    },
    {
      path: '/product',
      component: CreateProduct
    },
    {
      path: '/wizard',
      component: WizardExample
    },
    {
      path: '/login',
      component: Auth
    }
  ]
});

export default new Vue({
  el: '#root',
  router,
  render: h => h('router-view')
});
